import React from 'react'
import ig from '../images/ig.png';
import footer1 from '../images/footer1.png';
import footer2 from '../images/footer2.png';
import footer3 from '../images/footer3.png';
import footer5 from '../images/footer5.png';
import logo from '../images/logo.png';
function footer() {
    return (
        <div className='container footer my-5 d-flex justify-content-between flex-column flex-md-row gap-5'>
            <div className='d-flex flex-column'>
                <p>Jalan Suroyo No. 161 Mayangan Kota<br />Probolonggo 672000</p>
                <p className="mt-2">binarcarrental@gmail.com</p>
                <p className="mt-2">081-233-334-808</p>
            </div>
            <div className='d-flex flex-column'>
                <a href="#services" className="text-decoration-none text-dark">Our services</a>
                <a href="#whyus" className="text-decoration-none text-dark mt-3">Why Us</a>
                <a href="#testimonial" className="text-decoration-none text-dark mt-3">Testimonial</a>
                <a href="#faq" className="text-decoration-none text-dark mt-3">FAQ</a>
            </div>
            <div className='d-flex flex-column'>
                <p>Connect with us</p>
                <div className="flex gap-2">
                <img alt='ig' src={ig}/>
                <img alt='footer1' src={footer1}/>
                <img alt='footer2' src={footer2}/>
                <img alt='footer3' src={footer3}/>
                <img alt='footer4' src={footer5}/>
                </div>
            </div>
            <div>
                <p>© Copyright Binar 2022</p>
                <img alt='logo' src={logo}/>
            </div>
        </div>
    )
}

export default footer