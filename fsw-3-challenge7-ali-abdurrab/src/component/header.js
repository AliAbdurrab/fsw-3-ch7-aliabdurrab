import 'bootstrap/dist/css/bootstrap.min.css';
import mobil from '../images/mobil.png';

function header() {
    return (
        <div className='hero'>
            <div className="container">
                <div className="row d-flex align-items-center hero">
                    <div className="col-md-6">
                        <h1>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                        <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                        <a href='/cars' className="btn btn-primary" type="button">Mulai Sewa Mobil</a>
                    </div>
                    <div className="col-md-6 d-flex align-self-end">
                        <img className="img-hero img-fluid" alt='mobil' src={mobil} />
                    </div>
                </div>
            </div>
        </div>

    );
}

export default header;
